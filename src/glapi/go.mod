module glaber.io/glapi

go 1.13

require (
	github.com/ClickHouse/clickhouse-go v1.4.3
	github.com/aleasoluciones/goaleasoluciones v0.0.0-20200619085850-b0f5cadff0cb // indirect
	github.com/gosnmp/gosnmp v1.29.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fastjson v1.6.1
	gitlab.com/mikler/gosnmpquerier v0.0.0-20210107085610-0b27357ac29e // indirect
)
